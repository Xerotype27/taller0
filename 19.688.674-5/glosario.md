glosario

1. Control de versiones (VC): Se llama control de versiones a la gestion de los diversos cambios sobre los elementos de alguna configuracion del mismo. es el estado en el que                                 se encuentra el mismo en un momento dado de su desarrollo o modificación.

2. Control de versiones distribuido (DVC): En programación informática, el control de versiones distribuido permite a muchos desarrolladores de software trabajar en un proyecto                                            común sin necesidad de compartir una misma red. Las revisiones son almacenadas en un sistema de control de versiones distribuido                                                 (DVCS, por sus siglas en inglés).

3. Repositorio remoto y Repositorio local: Los repositorios remotos son versiones de tu proyecto que se encuentran alojados en Internet o en algún punto de la red. Puedes tener                                            varios, cada uno de los cuales puede ser de sólo lectura, o de lectura/escritura, según los permisos que tengas. Colaborar con otros                                             implica gestionar estos repositorios remotos, y mandar (push) y recibir (pull) datos de ellos cuando necesites compartir cosas. En                                               cambio el repositorio local es el cual se aloja en tu computador.

4. Copia de trabajo / Working Copy: Es una forma de guardar tu trabajo realizado, tras ejecutar la operacion checkout y haber creado esta copia de un repositorio local.

5. Área de Preparación / Staging Area: Es el area en el que se focalizaran sus cambios y modificaciones.

6. Preparar Cambios / Stage Changes: Es una etapa donde se preparan los cambios que se desean realizar al proyecto, utilizando el comando add para agregar estos cambios.

7. Confirmar cambios / Commit Changes: Se confirma el cambio que se realizo en el proyecto, utilizando el comando commit y dejando un mensaje opcional diciendo que cambios                                             fueron realizados en el.

8. Commit: Consolidar, confirmar​ o hacer un commit se refiere, en el contexto de la ciencia de la computación y la gestión de datos, a la idea de confirmar un conjunto de                 cambios provisionales de forma permanente. Un uso popular es al final de una transacción de base de datos.

9. clone: Es un comando en git el cual te permite importar el repositorio remoto.

10. pull: Comando utilizado para actualizar el repositorio local al commit mas nuevo.

11. push: Comando utilizado para enviar los cambios a tu repositorio remoto.

12. fetch: Comando utilizado para deshacer todos los cambios locales y commits.

13. merge: Comando utilizado para bajar y fusionar los cambios remotos.

14. status: Comando utilizado para  mostrar el estado del proyecto y los cambios que se le han realizado.

15. log: Comando utilizado para obtener el id de un commit.

16. checkout: Comando utilizado para reemplazar cambios locales, en caso de haber cometido un error.

17. Rama / Branch: Las ramas son utilizadas para desarrollar funcionalidades aisladas unas de otras. La rama master es la rama "por defecto" cuando creas un repositorio.

18. Etiqueta / Tag: Las etiquetas se recomiendan para cada nueva version publicada de un software. Es el nombre de la version.

